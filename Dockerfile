FROM centos:7
RUN yum -y install wget tar
RUN ls
WORKDIR /code
COPY . /code
RUN wget -O node-v16.18.0-linux-x64.tar.gz https://nodejs.org/dist/v16.18.0/node-v16.18.0-linux-x64.tar.gz


RUN tar zxvf node-v16.18.0-linux-x64.tar.gz
RUN mv node-v16.18.0-linux-x64 /usr/local/node
RUN ln -s /usr/local/node/bin/node /usr/local/bin/node
RUN ln -s /usr/local/node/bin/npm /usr/local/bin/npm

RUN npm install
RUN npm run build
EXPOSE 8080
CMD npm start